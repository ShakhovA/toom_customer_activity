select
    tmp.DWH_CUSTOMER_ID,
    to_char(tmp.MIN_DATE_TIME_START, 'DD-Mon-YYYY') as MIN_DATE_TIME_START,
    to_char(tmp.DATE_TIME_START_1, 'DD-Mon-YYYY') as DATE_TIME_START_1,
    
    cl.SEX_ID                                                           as GENDER,
    cl.NO_OF_CARDCOPIES                                                 as CARDS_NO,
    cl.CUST_STATUS                                                      as CHANNEL_ADS,
    cl.APP_RESIDENCETYPE                                                as RESIDENCE_TYPE,
    cl.SALUTATIONCODE_DESC                                              as SALUTATION,
    to_char(cl.BIRTH_DT, 'DD-Mon-YYYY')                                 as BIRTH_DATE,
    round(tmp.DISCOUNT_GROSS, 2)                                        as DISCOUNT_GROSS,
    round(aggbl.REVENUE_TOTAL, 2)                                       as REVENUE_GROSS,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_GARDEN/aggbl.REVENUE_TOTAL, 4) END                  as REVENUE_GARDEN_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_HOME/aggbl.REVENUE_TOTAL, 4) END                    as REVENUE_HOME_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_CONSTRUCTION/aggbl.REVENUE_TOTAL, 4) END            as REVENUE_CONSTRUCTION_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_DUMMY/aggbl.REVENUE_TOTAL, 4) END                   as REVENUE_DUMMY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_EXTENDED/aggbl.REVENUE_TOTAL, 4) END                as REVENUE_EXTENDED_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.00 ELSE round(aggbl.REVENUE_PROMO_ANY/aggbl.REVENUE_TOTAL, 4) END               as REVENUE_PROMO_ANY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_GARDEN/aggbl.REVENUE_TOTAL, 4) END            as REVENUE_LEVEL_GARDEN_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_SOFT_DIY/aggbl.REVENUE_TOTAL, 4) END          as REVENUE_LEVEL_SOFT_DIY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_HARD_DIY/aggbl.REVENUE_TOTAL, 4) END          as REVENUE_LEVEL_HARD_DIY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_OTHER/aggbl.REVENUE_TOTAL, 4) END             as REVENUE_LEVEL_OTHER_SHARE,
    
    aggbl.COUNT_PRODUCTS_DISTINCT,
    round(aggbl.FLAG_PROMO_ANY/aggbl.COUNT_PRODUCTS_DISTINCT, 4)        as PROMO_ITEMS_SHARE,
    
    round(aggbl.COUNT_ITEMS_GARDEN/aggbl.COUNT_PRODUCTS_DISTINCT, 4)    as ITEMS_GARDEN_SHARE,
    round(aggbl.COUNT_ITEMS_SOFT_DIY/aggbl.COUNT_PRODUCTS_DISTINCT, 4)  as ITEMS_SOFT_DIY_SHARE,
    round(aggbl.COUNT_ITEMS_HARD_DIY/aggbl.COUNT_PRODUCTS_DISTINCT, 4)  as ITEMS_HARD_DIY_SHARE,
    round(aggbl.COUNT_ITEMS_OTHER/aggbl.COUNT_PRODUCTS_DISTINCT, 4)     as ITEMS_OTHER_SHARE
    
from TMP_CUSTHEADER_2TRX_TEST tmp

inner join TOOM_CM_CORE.D_CUSTOMER cl
on tmp.DWH_CUSTOMER_ID = cl.DWH_CUSTOMER_ID

inner join TMP_CUST_AGGBL_1TRX_TEST aggbl
on tmp.DWH_CUSTOMER_ID = aggbl.DWH_CUSTOMER_ID
;