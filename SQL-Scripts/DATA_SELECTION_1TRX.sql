-------------------------Nur eine Transaktion----------------------------------------------------

/* 
DROP TABLE TMP_CUST_2TRX_DIFF_DAYS  PURGE;
CREATE TABLE TMP_CUST_2TRX_DIFF_DAYS
as
select
  b.DWH_CUSTOMER_ID	
, b.DWH_CALENDAR_DAY_ID	
, y.DATE_TIME_START
, count (b.DWH_CALENDAR_DAY_ID	)   as COUNT_TRX
from TOOM_CM_CORE.F_BON_HEADER b
inner join TOOM_CM_CORE.D_DATE y --Datumsmerkmale
     on  b.DWH_CALENDAR_DAY_ID = y.DWH_DATE_ID
--where b.DWH_CUSTOMER_ID in (1725128,1725808 )
group  by b.DWH_CUSTOMER_ID, b.DWH_CALENDAR_DAY_ID, y.DATE_TIME_START
order by b.DWH_CUSTOMER_ID	;
*/
--------Optimierung---------Der erste Kauf wird gesucht und der zweite um die Differenz als Zielvariable zu setzen------------------------------------------------------------------------------------

DROP TABLE TMP_CUST_2TRX_DIFF_DAYS  PURGE;
CREATE TABLE TMP_CUST_2TRX_DIFF_DAYS
as
select
  a.DWH_CUSTOMER_ID
, min (a.DATE_TIME_START)       as MIN_DATE_TIME_START
, min (a.DWH_CALENDAR_DAY_ID)   as MIN_DWH_CALENDAR_DAY_ID
, min (a.DATE_TIME_START_1)     as DATE_TIME_START_1
, min (a.DATE_TIME_START_2)     as DATE_TIME_START_2
, min (a.DATE_TIME_START_3)     as DATE_TIME_START_3

from (
select
    i.DWH_CUSTOMER_ID	
  , i.DATE_TIME_START
  , i.DWH_CALENDAR_DAY_ID
  , lead(DATE_TIME_START,1) over(partition by i.DWH_CUSTOMER_ID order by i.DATE_TIME_START asc) as DATE_TIME_START_1 
  , lead(DATE_TIME_START,2) over(partition by i.DWH_CUSTOMER_ID order by i.DATE_TIME_START asc) as DATE_TIME_START_2 
  , lead(DATE_TIME_START,3) over(partition by i.DWH_CUSTOMER_ID order by i.DATE_TIME_START asc) as DATE_TIME_START_3 
   from (
          select
            b.DWH_CUSTOMER_ID	
          , b.DWH_CALENDAR_DAY_ID	
          ,  to_date(b.DWH_CALENDAR_DAY_ID, 'YYYYMMDD') as  DATE_TIME_START
          --, count (b.DWH_CALENDAR_DAY_ID	)   as COUNT_TRX
          from TOOM_CM_CORE.F_BON_HEADER b    
          --where b.DWH_CUSTOMER_ID in (1725128,1725808 )
          group  by b.DWH_CUSTOMER_ID, b.DWH_CALENDAR_DAY_ID,  to_date(b.DWH_CALENDAR_DAY_ID, 'YYYYMMDD')
          order by b.DWH_CUSTOMER_ID	
    ) i
order by i.DATE_TIME_START
) a
group by a.DWH_CUSTOMER_ID;

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------Filter selktiere nur Kunden, die aktive sind und analysiert werden können. Auch releven erst ab 2013--------------------------------------------------------------------------


DROP TABLE TMP_CUST_2TRX_DAYS  PURGE;
CREATE TABLE TMP_CUST_2TRX_DAYS
as 
select
d.DWH_CUSTOMER_ID
, d.MIN_DATE_TIME_START
, d.MIN_DWH_CALENDAR_DAY_ID
, d.DATE_TIME_START_1
, d.DATE_TIME_START_2
, d.DATE_TIME_START_3
from TMP_CUST_2TRX_DIFF_DAYS d
inner join TOOM_CM_CORE.D_CUSTOMER cu
on d.DWH_CUSTOMER_ID = cu.DWH_CUSTOMER_ID
and cu.ANALYSIS_PERMIT_ID <> 0 
and cu.ACCOUNTSTATUS = 'aktiv'
and extract (year from d.MIN_DATE_TIME_START) > 2012;

--1.609.991
select
count (*)
from TMP_CUST_2TRX_DAYS ; 
--where DWH_CUSTOMER_ID = (1725128); 

---------------BONHEADER AGG AUS KUNDENEBENE--------------------------------------------------------
----------------------------------------------------------------------------------------------------
--------------------------AGG alle Werte auf eine Transaktion------------------------------------------------------
    DROP TABLE TMP_CUSTHEADER_21TRX_TEST  PURGE;
    CREATE TABLE TMP_CUSTHEADER_21TRX_TEST nologging parallel
    as
    select
      a.DWH_CUSTOMER_ID
    , max(a.DATE_TIME_START_1)                                                        as DATE_TIME_START_1
    , max(a.DATE_TIME_START_2)                                                        as DATE_TIME_START_2
    , max(a.DATE_TIME_START_3)                                                        as DATE_TIME_START_3
    , min(a.MIN_DATE_TIME_START)                                                      as MIN_DATE_TIME_START
    , max(a.MAX_DATE_TIME_START)                                                      as MAX_DATE_TIME_START
    , max(a.COUNT_TRX)                                                                as COUNT_TRX
    , max(a.REVENUE_GROSS)                                                            as REVENUE_GROSS
    , max (a.DISCOUNT_GROSS)                                                          as DISCOUNT_GROSS
    , max (a.DISCOUNT_GROSS_TOTAL)                                                    as DISCOUNT_GROSS_TOTAL
    , max (a.COUNT_TRX_DAYS)                                                          as COUNT_TRX_DAYS
   
    from
    (
    select
      b.DWH_CUSTOMER_ID
    , b.DWH_BON_HDR_ID
    , b.DWH_CALENDAR_DAY_ID
    , b.DWH_WORKING_HOUR_ID
    , b.DWH_TRANSACTION_DAY_ID
    , y.DATE_TIME_START
 
    , count(distinct b.DWH_BON_HDR_ID) over(partition by b.DWH_CUSTOMER_ID)                                                             as COUNT_TRX
    , count(distinct b.DWH_CALENDAR_DAY_ID) over(partition by b.DWH_CUSTOMER_ID)                                                        as COUNT_TRX_DAYS
    , sum(b.bon_revenue_gross) over(partition by b.DWH_CUSTOMER_ID)                                                                     as REVENUE_GROSS
    , sum(b.bon_w_pos_disc_gross) over(partition by b.DWH_CUSTOMER_ID)                                                                  as DISCOUNT_GROSS
    , sum(b.bon_w_disc_gross) over(partition by b.DWH_CUSTOMER_ID)                                                                      as DISCOUNT_GROSS_TOTAL
    , min(b.DWH_CALENDAR_DAY_ID) over(partition by b.DWH_CUSTOMER_ID )                                                                as MIN_DWH_CALENDAR_DAY_ID
    , max(b.DWH_CALENDAR_DAY_ID) over(partition by b.DWH_CUSTOMER_ID )                                                                as MAX_DWH_CALENDAR_DAY_ID
    , min(y.DATE_TIME_START) over(partition by b.DWH_CUSTOMER_ID )                                                                    as MIN_DATE_TIME_START
    , max(y.DATE_TIME_START) over(partition by b.DWH_CUSTOMER_ID )                                                                    as MAX_DATE_TIME_START
    , d.DATE_TIME_START_1                                                                                                             as DATE_TIME_START_1
    , d.DATE_TIME_START_2                                                                                                             as DATE_TIME_START_2
    , d.DATE_TIME_START_3

    from
      TOOM_CM_CORE.F_BON_HEADER b
      inner join TMP_CUST_2TRX_DAYS d
      on   b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID
      and  d.DATE_TIME_START_1 is not null --535275
     -- on  d.DWH_CUSTOMER_ID in (1725128,1725808 )-- 535275
    --  and b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID
     inner join TOOM_CM_CORE.D_DATE y --Datumsmerkmale
     on  b.DWH_CALENDAR_DAY_ID = y.DWH_DATE_ID
     and y.DATE_TIME_START = d.MIN_DATE_TIME_START
     
    ) a
    group by a.DWH_CUSTOMER_ID ;
     
  -------------------------------------------------------------------------------------------------------------------------------------
select
*
from TMP_CUSTHEADER_21TRX_TEST ;

--------------------------------Bonline Infos auch aggregiert auf eine Transaktion--------------------------------------------------------------------------
--TMP_CUST_BOND_RDATE_TEST
DROP TABLE TMP_CUST_BOND_1TRX_TEST  PURGE;
CREATE TABLE TMP_CUST_BOND_1TRX_TEST nologging parallel
as
select
   b.DWH_CUSTOMER_ID
 , b.DWH_CALENDAR_DAY_ID
 , y.DATE_TIME_START
 , sum ( b.POS_REVENUE_GROSS ) over(partition by b.DWH_CUSTOMER_ID)                                                                       as REVENUE_TOTAL
  ----Artikeln
 , sum ( case when  arc.IFH_LEVEL1_NO = '3' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_GARDEN
 , sum ( case when  arc.IFH_LEVEL1_NO = '1' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_HOME
 , sum ( case when  arc.IFH_LEVEL1_NO = '2' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_CONSTRUCTION
 , sum ( case when  arc.IFH_LEVEL1_NO = '99999' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)               as REVENUE_DUMMY
 , sum ( case when  arc.IFH_LEVEL1_NO = '4' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_EXTENDED
, max ( case when  str.DET_AGE_GROUP  in ('Schließmarkt', 'Schließung') then 0 else 1 end ) over(partition by b.DWH_CUSTOMER_ID)         as STORE_OPEN
, sum (case when b.POS_LIST_PROMO_ID ='0' then 0 else 1 end ) over(partition by b.DWH_CUSTOMER_ID)                                       as FLAG_PROMO_ANY -- Bonpos mit irgendeiner Promo-ID
, count(distinct DWH_bon_line_id) over(partition by b.DWH_CUSTOMER_ID)                                                                   as COUNT_PRODUCTS_DISTINCT
, count(distinct b.DWH_SALES_STORE_ID)    over(partition by b.DWH_CUSTOMER_ID)                                                           as COUNT_STORES_VISITED
 --Umsatz nach Kategorien
, sum(case when b.POS_LIST_PROMO_ID <>'0' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                     as REVENUE_PROMO_ANY
, sum(case when b.pos_promotion_flag=1 then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                        as REVENUE_PROMO_TOTAL
--, ct.level0_NO
--Umsatz CM-Level0
, sum(case when ct.level0_NO='03' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                            as REVENUE_LEVEL_GARDEN   --GARTEN
, sum(case when ct.level0_NO='02' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                            as REVENUE_LEVEL_SOFT_DIY --SOFT DIY
, sum(case when ct.level0_NO='01' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                            as REVENUE_LEVEL_HARD_DIY --HARD DIY
, sum(case when ct.level0_NO='NA' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                            as REVENUE_LEVEL_OTHER    --sonstige
--Bonpos CM-Level0
, sum(case when ct.level0_NO='03' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                              as COUNT_ITEMS_GARDEN   --GARTEN
, sum(case when ct.level0_NO='02' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                              as COUNT_ITEMS_SOFT_DIY --SOFT DIY
, sum(case when ct.level0_NO='01' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                              as COUNT_ITEMS_HARD_DIY --HARD DIY
, sum(case when ct.level0_NO='NA' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                              as COUNT_ITEMS_OTHER    --sonstige
--Bontrx CM-Level0
, count(distinct case when ct.level0_NO='03' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                         as COUNT_TRX_GARDEN   --GARTEN
, count(distinct case when ct.level0_NO='02' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                         as COUNT_TRX_SOFT_DIY --SOFT DIY
, count(distinct case when ct.level0_NO='01' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                         as COUNT_TRX_HARD_DIY --HARD DIY
, count(distinct case when ct.level0_NO='NA' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                         as COUNT_TRX_OTHER    --sonstige
from  DS_F_BON_LINE b
      inner join TMP_CUSTHEADER_21TRX_TEST d
      on b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID
     inner join TOOM_CM_CORE.D_DATE y
     on  b.DWH_CALENDAR_DAY_ID = y.DWH_DATE_ID
     and y.DATE_TIME_START = d.MIN_DATE_TIME_START  --first buydate
     inner join TOOM_CM_CORE.D_SALES_STORE str
     on b.DWH_SALES_STORE_ID  = str.DWH_SALES_STORE_ID
     inner join TOOM_CM_CORE.D_NAN_ARTICLE arc
     on b.DWH_NAN_ART_ID = arc.DWH_NAN_ART_ID
     inner join TOOM_CM_CORE.D_CATEGORY_MANAGEMENT  ct
     on b.CM_LEVEL2_ID = ct.LEVEL2_ID ;

-------------------------------------Subselect um den Druchlauf etwas zu beschleunigen---------------------------------------------------------------------


select
count(*)
from TMP_CUST_BOND_1TRX_TEST ;
DROP TABLE TMP_CUST_AGGBL_1TRX_TEST  PURGE;
 CREATE TABLE TMP_CUST_AGGBL_1TRX_TEST nologging parallel
as
--with selection_date as
  
select
  a.DWH_CUSTOMER_ID  
, max (a.REVENUE_TOTAL)                                                         as REVENUE_TOTAL
, max (a.REVENUE_GARDEN)                                                        as REVENUE_GARDEN 
--divided by number of years 
, max(a.REVENUE_HOME)                                                           as REVENUE_HOME
, max(a.REVENUE_CONSTRUCTION)                                                   as REVENUE_CONSTRUCTION
, max(a.REVENUE_DUMMY)                                                          as REVENUE_DUMMY
, max(a.REVENUE_EXTENDED)                                                       as REVENUE_EXTENDED

, max(a.REVENUE_LEVEL_OTHER )                                                   as REVENUE_LEVEL_OTHER
, max(a.REVENUE_LEVEL_GARDEN)                                                   as REVENUE_LEVEL_GARDEN
, max(a.REVENUE_LEVEL_SOFT_DIY)                                                 as REVENUE_LEVEL_SOFT_DIY 
, max(a.REVENUE_LEVEL_HARD_DIY)                                                 as REVENUE_LEVEL_HARD_DIY 

, max(a.STORE_OPEN)                                                             as STORE_OPEN
, max (a.COUNT_TRX_GARDEN)                                                      as COUNT_TRX_GARDEN
, max (a.COUNT_TRX_HARD_DIY)                                                    as COUNT_TRX_HARD_DIY
, max (a.COUNT_TRX_SOFT_DIY)                                                    as COUNT_TRX_SOFT_DIY
, max (a.COUNT_TRX_OTHER)                                                       as COUNT_TRX_OTHER

, max (a.COUNT_ITEMS_GARDEN)                                                    as COUNT_ITEMS_GARDEN
, max (a.COUNT_ITEMS_SOFT_DIY)                                                  as COUNT_ITEMS_SOFT_DIY
, max (a.COUNT_ITEMS_HARD_DIY)                                                  as COUNT_ITEMS_HARD_DIY
, max (a.COUNT_ITEMS_OTHER)                                                     as COUNT_ITEMS_OTHER
, max( a.COUNT_PRODUCTS_DISTINCT)                                               as COUNT_PRODUCTS_DISTINCT
, max( a.FLAG_PROMO_ANY)                                                        as FLAG_PROMO_ANY
, max( a.REVENUE_PROMO_TOTAL)                                                   as REVENUE_PROMO_TOTAL
, max( a.REVENUE_PROMO_ANY)                                                     as REVENUE_PROMO_ANY


from  TMP_CUST_BOND_1TRX_TEST a
group by  a.DWH_CUSTOMER_ID ;

-------------------------------------------Test der Ergebnisse----------------------------------------------------------------------------------------------

select
*
from TMP_CUSTHEADER_21TRX_TEST d
left join  TMP_CUST_AGGBL_1TRX_TEST h
on d.DWH_CUSTOMER_ID = h.DWH_CUSTOMER_ID

where h.DWH_CUSTOMER_ID is null;


--------------------------------------------------------------------------------------------------------------------------------------------------------------
--Liste der Kunden die analysiert werden sollen------------Daniels Join soll neue Tabelle werden?----------------------------------------------------------------------------------------
DROP TABLE TMP_1TRX_TOTAL PURGE;
CREATE TABLE TMP_1TRX_TOTAL NOLOGGING PARALLEL 
AS
select
    tmp.DWH_CUSTOMER_ID,
    to_char(tmp.MIN_DATE_TIME_START, 'DD-Mon-YYYY')                     as MIN_DATE_TIME_START,
    to_char(tmp.DATE_TIME_START_1, 'DD-Mon-YYYY')                       as DATE_TIME_START_1,
    cl.SEX_ID                                                           as GENDER,
    cl.NO_OF_CARDCOPIES                                                 as CARDS_NO,
    cl.CUST_STATUS                                                      as CHANNEL_ADS,
  --  cl.APP_RESIDENCETYPE                                                as RESIDENCE_TYPE,
    cl.SALUTATIONCODE_DESC                                              as SALUTATION,
    to_char(cl.BIRTH_DT, 'DD-Mon-YYYY')                                 as BIRTH_DATE,
    round(tmp.DISCOUNT_GROSS, 2)                                        as DISCOUNT_GROSS,
 --   round(tmp.DISCOUNT_GROSS_TOTAL, 2)                                  as DISCOUNT_GROSS_TOTAL,-- soll die neue Variable rain? 
    round(aggbl.REVENUE_TOTAL, 2)                                       as REVENUE_GROSS,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_GARDEN/aggbl.REVENUE_TOTAL, 4) END                  as REVENUE_GARDEN_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_HOME/aggbl.REVENUE_TOTAL, 4) END                    as REVENUE_HOME_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_CONSTRUCTION/aggbl.REVENUE_TOTAL, 4) END            as REVENUE_CONSTRUCTION_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_DUMMY/aggbl.REVENUE_TOTAL, 4) END                   as REVENUE_DUMMY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.20 ELSE round(aggbl.REVENUE_EXTENDED/aggbl.REVENUE_TOTAL, 4) END                as REVENUE_EXTENDED_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.00 ELSE round(aggbl.REVENUE_PROMO_ANY/aggbl.REVENUE_TOTAL, 4) END               as REVENUE_PROMO_ANY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_GARDEN/aggbl.REVENUE_TOTAL, 4) END            as REVENUE_LEVEL_GARDEN_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_SOFT_DIY/aggbl.REVENUE_TOTAL, 4) END          as REVENUE_LEVEL_SOFT_DIY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_HARD_DIY/aggbl.REVENUE_TOTAL, 4) END          as REVENUE_LEVEL_HARD_DIY_SHARE,
    CASE WHEN aggbl.REVENUE_TOTAL = 0 THEN 0.25 ELSE round(aggbl.REVENUE_LEVEL_OTHER/aggbl.REVENUE_TOTAL, 4) END             as REVENUE_LEVEL_OTHER_SHARE,
    aggbl.COUNT_PRODUCTS_DISTINCT,
    round(aggbl.FLAG_PROMO_ANY/aggbl.COUNT_PRODUCTS_DISTINCT, 4)        as PROMO_ITEMS_SHARE,
    round(aggbl.COUNT_ITEMS_GARDEN/aggbl.COUNT_PRODUCTS_DISTINCT, 4)    as ITEMS_GARDEN_SHARE,
    round(aggbl.COUNT_ITEMS_SOFT_DIY/aggbl.COUNT_PRODUCTS_DISTINCT, 4)  as ITEMS_SOFT_DIY_SHARE,
    round(aggbl.COUNT_ITEMS_HARD_DIY/aggbl.COUNT_PRODUCTS_DISTINCT, 4)  as ITEMS_HARD_DIY_SHARE,
    round(aggbl.COUNT_ITEMS_OTHER/aggbl.COUNT_PRODUCTS_DISTINCT, 4)     as ITEMS_OTHER_SHARE
    
from TMP_CUSTHEADER_21TRX_TEST tmp  
inner join TOOM_CM_CORE.D_CUSTOMER cl
on tmp.DWH_CUSTOMER_ID = cl.DWH_CUSTOMER_ID
inner join TMP_CUST_AGGBL_1TRX_TEST aggbl
on tmp.DWH_CUSTOMER_ID = aggbl.DWH_CUSTOMER_ID
;
























