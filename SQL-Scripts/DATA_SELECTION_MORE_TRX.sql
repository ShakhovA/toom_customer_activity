/*---------------------------Start with the ranges
*/
-----------------------------------Selektiere nur die Kuden, die aktiv sind und analysiert werden können-------------------------------------------
DROP TABLE TMP_RANDOMVALUE1_TEST  PURGE; 
exec DBMS_RANDOM.seed (val => 0)
CREATE TABLE TMP_RANDOMVALUE1_TEST nologging parallel
    as
    SELECT
      distinct cu.DWH_CUSTOMER_ID
    , floor(dbms_random.value(1,365))        as RANDOM_NUMBER
       
 FROM TOOM_CM_CORE.D_CUSTOMER cu
    WHERE cu.ANALYSIS_PERMIT_ID <> 0 
    AND   cu.ACCOUNTSTATUS = 'aktiv'   ;
    
  --1.603.775
    select
    count( * )  ---2.503.302
    from TMP_RANDOMVALUE1_TEST;
    

------------------------SYSDATE -FIXEN-------------------------------------------------
---------------------------Speichert alle zufällige Zahlen pro Kunde-------------------
    --
    DROP TABLE TMP_RANDOMDATES1_TEST  PURGE;
    CREATE TABLE TMP_RANDOMDATES1_TEST nologging parallel
    as
    SELECT t.DWH_CUSTOMER_ID      
    , t.RANDOM_NUMBER 
    , TRUNC(SYSDATE)                                                                as    DATE_BEFORE_TO
    , TRUNC(TO_DATE('2014-01-01', 'yyyy-MM-dd'))  - t.RANDOM_NUMBER                 as    RANGE_FROM
    , TRUNC(TO_DATE('2020-02-27', 'yyyy-MM-dd'))  - t.RANDOM_NUMBER                 as    RANGE_TO
    FROM TMP_RANDOMVALUE1_TEST t;
    
    select
    count( *) -- count (*)  ---1.963.761
    from TMP_RANDOMDATES1_TEST;
-----------------------------------------------------------------------------------------------------------
------------------------------Gruppiert jede Transaktion by Tag und Kunde----------------------------------
DROP TABLE TMP_CUST_T2DATES_DAYS  PURGE;
CREATE TABLE TMP_CUST_T2DATES_DAYS
as
select
  b.DWH_CUSTOMER_ID	
, b.DWH_CALENDAR_DAY_ID	
, y.DATE_TIME_START	
, max( d.RANGE_FROM)        as RANGE_FROM
, max (d.RANGE_TO )         as RANGE_TO

from TOOM_CM_CORE.F_BON_HEADER b
inner join TMP_RANDOMDATES1_TEST d

on b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID     
inner join TOOM_CM_CORE.D_DATE y --Datumsmerkmale
on  b.DWH_CALENDAR_DAY_ID = y.DWH_DATE_ID
and y.DATE_TIME_START >= d.RANGE_FROM
and y.DATE_TIME_START <= d.RANGE_TO 
group by  b. DWH_CUSTOMER_ID , b.DWH_CALENDAR_DAY_ID, y.DATE_TIME_START ;
    
select
*
from TMP_CUST_T2DATES_DAYS;
 
------------------------Berechne die letzten 4 Käufe aller Kunden--------------------------------------------------
DROP TABLE TMP_CUST_TRX_DIFF1_DAYS  PURGE;
CREATE TABLE TMP_CUST_TRX_DIFF1_DAYS nologging parallel
as
select
  DWH_CUSTOMER_ID		
, DATE_TIME_START	
, RANGE_FROM	
, RANGE_TO
, lead(DATE_TIME_START,1) over(partition by DWH_CUSTOMER_ID order by DATE_TIME_START desc) as DATE_TIME_START_prev1
, lead(DATE_TIME_START,2) over(partition by DWH_CUSTOMER_ID order by DATE_TIME_START desc) as DATE_TIME_START_prev2
, lead(DATE_TIME_START,3) over(partition by DWH_CUSTOMER_ID order by DATE_TIME_START desc) as DATE_TIME_START_prev3
, lead(DATE_TIME_START,4) over(partition by DWH_CUSTOMER_ID order by DATE_TIME_START desc) as DATE_TIME_START_prev4

from TMP_CUST_T2DATES_DAYS 
order by DWH_CUSTOMER_ID; 

select
count (*)
from TMP_CUST_TRX_DIFF1_DAYS ;

------------------------------------------------Subselect : AGG des Datums auf Kundenebene------------------------------------------------------------------
DROP TABLE TMP_CUST_TRX_LASTDIFF1_DAYS  PURGE;
CREATE TABLE TMP_CUST_TRX_LASTDIFF1_DAYS nologging parallel
as
select
  DWH_CUSTOMER_ID	
, max(DATE_TIME_START)                as MAX_DATE_TIME_START
, min(DATE_TIME_START)                as MIN_DATE_TIME_START
, max(RANGE_FROM)                     as RANGE_FROM	
, max(RANGE_TO)                       as RANGE_TO
, max(DATE_TIME_START_PREV1	)         as MAX_DATE_TIME_START_PREV1
, max(DATE_TIME_START_PREV2	)         as MAX_DATE_TIME_START_PREV2
, max(DATE_TIME_START_PREV3	)         as MAX_DATE_TIME_START_PREV3
, max(DATE_TIME_START_PREV4	)         as MAX_DATE_TIME_START_PREV4
, max(DATE_TIME_START)          -  max(DATE_TIME_START_PREV1	)  as TRX_DIFF_DAYS1	
, max(DATE_TIME_START_PREV1)    -  max(DATE_TIME_START_PREV2	)  as  TRX_DIFF_DAYS2	
, max(DATE_TIME_START_PREV2)    -  max(DATE_TIME_START_PREV3	)  as  TRX_DIFF_DAYS3	
, max(DATE_TIME_START_PREV3)    -  max(DATE_TIME_START_PREV4	)  as  TRX_DIFF_DAYS4

from TMP_CUST_TRX_DIFF1_DAYS
group by DWH_CUSTOMER_ID;

select
*
from TMP_CUST_TRX_LASTDIFF1_DAYS ;
--------------------------------------Bonheader infos auf Kundenebene: Selektion ab vorletzem Kauf------------------------------------------------------------------------------------
  
DROP TABLE TMP_CUSTHEADER_T1_TEST  PURGE;
CREATE TABLE TMP_CUSTHEADER_T1_TEST nologging parallel
as
      select 
      b.DWH_CUSTOMER_ID
    , b.DWH_BON_HDR_ID
    , b.DWH_CALENDAR_DAY_ID
    , b.DWH_WORKING_HOUR_ID
    , b.DWH_TRANSACTION_DAY_ID
    , y.DATE_TIME_START  
    , count( distinct EXTRACT(YEAR FROM y.DATE_TIME_START )) over (partition by b.DWH_CUSTOMER_ID )                                     as COUNT_ACTIVE_YEARS
    , count( distinct EXTRACT(MONTH FROM y.DATE_TIME_START )) over (partition by b.DWH_CUSTOMER_ID )                                    as COUNT_ACTIVE_MONTHS
    , round(MONTHS_BETWEEN (TO_DATE(d.MAX_DATE_TIME_START_PREV1,'DD-MM-YYYY'), TO_DATE(d.RANGE_FROM,'DD-MM-YYYY') ), 1)                 as MONTH_DIFFERENCE_RANGE
    , count ( distinct b.DWH_SALES_STORE_ID ) over (partition by b.DWH_CUSTOMER_ID )                                                    as COUNT_STORES  
    , count(distinct b.DWH_BON_HDR_ID) over(partition by b.DWH_CUSTOMER_ID)                                                             as COUNT_TRX   
    , count(distinct b.DWH_CALENDAR_DAY_ID) over(partition by b.DWH_CUSTOMER_ID)                                                        as COUNT_TRX_DAYS 
    , sum(b.bon_revenue_gross) over(partition by b.DWH_CUSTOMER_ID)                                                                     as REVENUE_GROSS
    , sum(b.bon_w_pos_disc_gross) over(partition by b.DWH_CUSTOMER_ID)                                                                  as DISCOUNT_GROSS  
    , sum(b.bon_w_disc_gross) over(partition by b.DWH_CUSTOMER_ID)                                                                      as DISCOUNT_GROSS_TOTAL
    , sum (b.RETOURE_FLAG ) over(partition by b.DWH_CUSTOMER_ID)                                                                        as COUNT_RETURNS
    , rank() over(partition by b.DWH_CUSTOMER_ID order by y.DATE_TIME_START)                                                            as RANK_BONID
    , rank() over(partition by b.DWH_CUSTOMER_ID order by y.DATE_TIME_START desc)                                                       as DENSE_RANK_BONID
    , min(b.DWH_CALENDAR_DAY_ID) over(partition by b.DWH_CUSTOMER_ID )                                                                  as MIN_DWH_CALENDAR_DAY_ID
    , max(b.DWH_CALENDAR_DAY_ID) over(partition by b.DWH_CUSTOMER_ID )                                                                  as MAX_DWH_CALENDAR_DAY_ID
    , min(y.DATE_TIME_START) over(partition by b.DWH_CUSTOMER_ID )                                                                      as MIN_DATE_TIME_START
    , max(y.DATE_TIME_START) over(partition by b.DWH_CUSTOMER_ID )                                                                      as MAX_DATE_TIME_START
    , d.RANGE_FROM                                                                                                                      as RANGE_FROM
    , d.RANGE_TO                                                                                                                        as RANGE_TO
    , d.MAX_DATE_TIME_START_PREV1                                                                                                       as CUT_DATE
    ----------------Zeitpunk Käufe-------------------------------------------------------------------
 --sollen wir das Wochenende betrachten 
 --  , sum (y.WEEKEND_FLAG ) over(partition by b.DWH_CUSTOMER_ID)                                                               as COUNT_DAYS_WEEKEND
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'MO' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_MON_DISTINCT
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'DI' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_TUE_DISTINCT
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'MI' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_WED_DISTINCT
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'DO' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_THU_DISTINCT
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'FR' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_FRI_DISTINCT
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'SA' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_SAT_DISTINCT
   , count (distinct case when y.DAY_OF_WEEK_SDESC = 'SO' then b.DWH_CALENDAR_DAY_ID else null end ) over (partition by b.DWH_CUSTOMER_ID)                      as COUNT_SUN_DISTINCT
  
  --sollen wir das Quartla betrachten
   , count (distinct case when y.QUARTER_DESC = 'Q1' then EXTRACT(YEAR FROM y.DATE_TIME_START )  else null end ) over (partition by b.DWH_CUSTOMER_ID)          as COUNT_Q1_DISTINCT
   , count (distinct case when y.QUARTER_DESC = 'Q2' then EXTRACT(YEAR FROM y.DATE_TIME_START )  else null end ) over (partition by b.DWH_CUSTOMER_ID)          as COUNT_Q2_DISTINCT
   , count (distinct case when y.QUARTER_DESC = 'Q3' then EXTRACT(YEAR FROM y.DATE_TIME_START)  else null end ) over (partition by b.DWH_CUSTOMER_ID)           as COUNT_Q3_DISTINCT
   , count (distinct case when y.QUARTER_DESC = 'Q4' then EXTRACT(YEAR FROM y.DATE_TIME_START )  else null end ) over (partition by b.DWH_CUSTOMER_ID)          as COUNT_Q4_DISTINCT
  --Anzahl von Tagen die in einem Quartal gekauft wurde
   
    from
      TOOM_CM_CORE.F_BON_HEADER b
    inner join TMP_CUST_TRX_LASTDIFF1_DAYS d
     on b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID     
     inner join TOOM_CM_CORE.D_DATE y --Datumsmerkmale
     on  b.DWH_CALENDAR_DAY_ID = y.DWH_DATE_ID
     and y.DATE_TIME_START >= d.RANGE_FROM
     and y.DATE_TIME_START <= d.MAX_DATE_TIME_START_PREV1 ;
   
------------------BONHEADER AGG AUF KUNDENEBENE---------------------------------------------------------------
------------------------------Subselect ----------------------------------------------------------------------
select
count (* )
from TMP_CUSTHEADER_T1_TEST;
    
DROP TABLE TMP_CUSTHEADER_T2_TEST  PURGE;
CREATE TABLE TMP_CUSTHEADER_T2_TEST nologging parallel
    as
    select
      a.DWH_CUSTOMER_ID
    , max(a.COUNT_ACTIVE_YEARS)                                                       as COUNT_ACTIVE_YEARS
    , max(a.COUNT_ACTIVE_MONTHS)                                                      as COUNT_ACTIVE_MONTHS
    , max(a.MONTH_DIFFERENCE_RANGE)                                                   as MONTH_DIFFERENCE_RANGE  
    , max (a.COUNT_RETURNS)                                                           as COUNT_RETURNS    
    , min(a.MIN_DATE_TIME_START)                                                      as MIN_DATE_TIME_START
    , max(a.MAX_DATE_TIME_START)                                                      as MAX_DATE_TIME_START  
    , max(a.COUNT_TRX)                                                                as COUNT_TRX     
    , max(a.REVENUE_GROSS)                                                            as REVENUE_GROSS     
    , max (a.DISCOUNT_GROSS)                                                          as DISCOUNT_GROSS   
    , max( a.DISCOUNT_GROSS_TOTAL)                                                    as DISCOUNT_GROSS_TOTAL
    , max(a.COUNT_STORES )                                                            as COUNT_STORES     
    , case when (max(a.COUNT_TRX)-1)=0 then 0 else (max(a.MAX_DATE_TIME_START)- min(a.DATE_TIME_START))/(max(a.COUNT_TRX)-1)end as AVG_ACTIVE_DIFF_DAYS 
    , max (a.COUNT_TRX_DAYS)                                                          as COUNT_TRX_DAYS     
    , max(a.RANGE_FROM)                                                               as RANGE_FROM
    , max(a.RANGE_TO)                                                                 as RANGE_TO   
    , max (a.COUNT_MON_DISTINCT)                                                     as COUNT_MON_DISTINCT
    , max (a.COUNT_TUE_DISTINCT)                                                     as COUNT_TUE_DISTINCT
    , max (a.COUNT_WED_DISTINCT)                                                     as COUNT_WED_DISTINCT
    , max (a.COUNT_THU_DISTINCT)                                                     as COUNT_THU_DISTINCT
    , max (a.COUNT_FRI_DISTINCT)                                                     as COUNT_FRI_DISTINCT
    , max (a.COUNT_SAT_DISTINCT)                                                     as COUNT_SAT_DISTINCT
    , max (a.COUNT_SUN_DISTINCT)                                                     as COUNT_SUN_DISTINCT
    , max (a.COUNT_Q1_DISTINCT)                                                      as COUNT_Q1_DISTINCT
    , max (a.COUNT_Q2_DISTINCT)                                                      as COUNT_Q2_DISTINCT
    , max (a.COUNT_Q3_DISTINCT)                                                      as COUNT_Q3_DISTINCT
    , max (a.COUNT_Q4_DISTINCT)                                                      as COUNT_Q4_DISTINCT
    , max(a.CUT_DATE)                                                                as CUT_DATE
  
 from TMP_CUSTHEADER_T1_TEST a
 group by a.DWH_CUSTOMER_ID ;

select
count(*) 
from TMP_CUSTHEADER_T2_TEST ;

     
 ---------------BONLINE ERSTER SCHRITT, BERECHNUNG AUF KUNDENEBENE: Selektion ab vor letztem Kauf----------------------------------------------
 ---------------------------------------------------------------------------------------------------------------
DROP TABLE TMP_CUST_BOND_RDATE_TEST  PURGE;
CREATE TABLE TMP_CUST_BOND_RDATE_TEST nologging parallel
as
 
select
   b.DWH_CUSTOMER_ID
 , b.DWH_CALENDAR_DAY_ID
 , y.DATE_TIME_START
  ----Artikeln
 , sum ( case when  arc.IFH_LEVEL1_NO = '3' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_GARDEN
 , sum ( case when  arc.IFH_LEVEL1_NO = '1' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_HOME
 , sum ( case when  arc.IFH_LEVEL1_NO = '2' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_CONSTRUCTION
 , sum ( case when  arc.IFH_LEVEL1_NO = '99999' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)               as REVENUE_DUMMY
 , sum ( case when  arc.IFH_LEVEL1_NO = '4' then b.POS_REVENUE_GROSS else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                   as REVENUE_EXTENDED
  
, max ( case when  str.DET_AGE_GROUP  in ('Schließmarkt', 'Schließung') then 0 else 1 end ) over(partition by b.DWH_CUSTOMER_ID)         as STORE_OPEN 
                                                                                                            
, sum (case when b.POS_LIST_PROMO_ID ='0' then 0 else 1 end ) over(partition by b.DWH_CUSTOMER_ID)                                      as FLAG_PROMO_ANY -- Bonpos mit irgendeiner Promo-ID
, count(distinct DWH_bon_line_id) over(partition by b.DWH_CUSTOMER_ID)                                                                  as COUNT_PRODUCTS_DISTINCT

, count(distinct b.DWH_SALES_STORE_ID)    over(partition by b.DWH_CUSTOMER_ID)                                                          as COUNT_STORES_VISITED
 --Umsatz nach Kategorien
, sum(case when b.POS_LIST_PROMO_ID <>'0' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                    as REVENUE_PROMO_ANY
, sum(case when b.pos_promotion_flag=1 then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                       as REVENUE_PROMO_TOTAL
--, ct.level0_NO
--Umsatz CM-Level0
, sum(case when ct.level0_NO='03' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                          as REVENUE_LEVEL_GARDEN   --GARTEN
, sum(case when ct.level0_NO='02' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                          as REVENUE_LEVEL_SOFT_DIY --SOFT DIY
, sum(case when ct.level0_NO='01' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                          as REVENUE_LEVEL_HARD_DIY --HARD DIY
, sum(case when ct.level0_NO='NA' then b.POS_REVENUE_GROSS ELSE 0 end ) over(partition by b.DWH_CUSTOMER_ID)                          as REVENUE_LEVEL_OTHER    --sonstige

--Bonpos CM-Level0
, sum(case when ct.level0_NO='03' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                          as COUNT_ITEMS_GARDEN   --GARTEN
, sum(case when ct.level0_NO='02' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                          as COUNT_ITEMS_SOFT_DIY --SOFT DIY
, sum(case when ct.level0_NO='01' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                          as COUNT_ITEMS_HARD_DIY --HARD DIY
, sum(case when ct.level0_NO='NA' then 1 else 0 end ) over(partition by b.DWH_CUSTOMER_ID)                                          as COUNT_ITEMS_OTHER    --sonstige

--Bontrx CM-Level0
, count(distinct case when ct.level0_NO='03' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                     as COUNT_TRX_GARDEN   --GARTEN
, count(distinct case when ct.level0_NO='02' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                     as COUNT_TRX_SOFT_DIY --SOFT DIY
, count(distinct case when ct.level0_NO='01' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                     as COUNT_TRX_HARD_DIY --HARD DIY
, count(distinct case when ct.level0_NO='NA' then b.DWH_BON_HDR_ID   end ) over(partition by b.DWH_CUSTOMER_ID)                     as COUNT_TRX_OTHER    --sonstige

                                                                                                       
from  DS_F_BON_LINE b    
    
      inner join TMP_CUST_TRX_LASTDIFF1_DAYS d
      on b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID   

     inner join TOOM_CM_CORE.D_DATE y 
     on  b.DWH_CALENDAR_DAY_ID = y.DWH_DATE_ID
     and y.DATE_TIME_START >= d.RANGE_FROM
     and y.DATE_TIME_START <= d.MAX_DATE_TIME_START_PREV1
     
     inner join TOOM_CM_CORE.D_SALES_STORE str 
     on b.DWH_SALES_STORE_ID  = str.DWH_SALES_STORE_ID
     
     inner join TOOM_CM_CORE.D_NAN_ARTICLE arc 
     on b.DWH_NAN_ART_ID = arc.DWH_NAN_ART_ID 
     
     inner join TOOM_CM_CORE.D_CATEGORY_MANAGEMENT  ct
     on b.CM_LEVEL2_ID = ct.LEVEL2_ID ;


---------------BONLINE AGG AUF KUNDENEBENE NACH BERECHNUNG ----------------------------

select
count(*)
from TMP_CUST_BOND_RDATE_TEST;


 DROP TABLE TMP_CUSTAGG_BOND_RDATE_TEST  PURGE;
 CREATE TABLE TMP_CUSTAGG_BOND_RDATE_TEST nologging parallel
as
  
select
  a.DWH_CUSTOMER_ID   
, max (a.REVENUE_GARDEN)                                                        as REVENUE_GARDEN 
, max(a.REVENUE_HOME)                                                           as REVENUE_HOME
, max(a.REVENUE_CONSTRUCTION)                                                   as REVENUE_CONSTRUCTION
, max(a.REVENUE_DUMMY)                                                          as REVENUE_DUMMY
, max(a.REVENUE_EXTENDED)                                                       as REVENUE_EXTENDED
, max(a.STORE_OPEN)                                                             as STORE_OPEN
from  TMP_CUST_BOND_RDATE_TEST a
group by  a.DWH_CUSTOMER_ID ;

-----------------------NACH DER AGGREGATION-------------------------------------------------------------------------------------------
select
*
from TMP_CUSTAGG_BOND_RDATE_TEST ;

----------------------D_CUST ZUSATZ-INFORMATION---------------------------------------------------------------------------------------
DROP TABLE TMP_CUST_ADDFEATURES_TEST PURGE;
CREATE TABLE TMP_CUST_ADDFEATURES_TEST nologging parallel
as
 select
        tmp.*
        , cl.SEX_ID                                                              as GENDER
        , cl.NO_OF_CARDCOPIES                                                    as CARDS_NO
        , cl.CUST_STATUS                                                         as CHANNEL_ADS
        , cl.APP_RESIDENCETYPE                                                   as RESIDENCE_TYPE
        , cl.SALUTATIONCODE_DESC                                                 as SALUTATION
     --   , round(months_between(tmp.CUT_DATE, cl.APP_APPLICATION_DATE),0)         as KUNDE_SEIT_MO
        , cl.APP_APPLICATION_DATE                                                as START_DATE
        , cl.LAST_RELOCATE_DT                                                    as LAST_RELOCATED_DATE
 from TMP_CUSTHEADER_T2_TEST   tmp
 inner join TOOM_CM_CORE.D_CUSTOMER  cl
 on tmp.DWH_CUSTOMER_ID = cl.DWH_CUSTOMER_ID
 and  cl.ANALYSIS_PERMIT_ID <> 0
 and ACCOUNTSTATUS = 'aktiv';
 
---------------------------------------------------DEADFLAG wird hinzugefügt---------------------------------------------------------------------------------------- 
DROP TABLE TMP_CUST_DEADFLAG_RDATE_TEST PURGE;
CREATE TABLE TMP_CUST_DEADFLAG_RDATE_TEST nologging parallel
as
select
 tmp.*
, case when  dead. DWH_CUSTOMER_ID is not null then 1 else 0 end  as DEAD_FLAG
from TMP_CUST_ADDFEATURES_TEST tmp
left join TMP_DEADCUS_DETAIL_TEST dead
on  tmp.DWH_CUSTOMER_ID = dead.DWH_CUSTOMER_ID ;

select
count (*) --1.641.968
from TMP_CUST_DEADFLAG_RDATE_TEST;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------NPS-WERTE--------------------------------------------------------------------------------------------------------------------------------------------------
DROP TABLE TMP_CUST_NPS_TEST  PURGE;
CREATE TABLE TMP_CUST_NPS_TEST nologging parallel
as
  select
    max ( a.RATE_CODE )         as MAX_RATE_CODE
  , min ( a.RATE_CODE )         as MIN_RATE_CODE
  , a.DWH_CUSTOMER_ID
  , max( a.RESP_TIMESTAMP_DAY ) as MAX_RESP_TIMESTAMP_DAY
  , min( a.RESP_TIMESTAMP_DAY ) as MIN_RESP_TIMESTAMP_DAY
  from (
 select
  tp.ACTIVITY_ID as DWH_ACTIVITY_ID
  , tp.CUST_ID as DWH_CUSTOMER_ID
  , tp.RESPLINK as RATING
  , case when tp.RESPLINK like'%zufrieden_9%' then 9
    when tp.RESPLINK like'%zufrieden_10%' then 10
    when tp.RESPLINK like'%zufrieden_8%' then 8
    when tp.RESPLINK like'%zufrieden_7%' then 7
    when tp.RESPLINK like'%zufrieden_6%' then 6
    when tp.RESPLINK like'%zufrieden_5%' then 5
    when tp.RESPLINK like'%zufrieden_4%' then 4
    when tp.RESPLINK like'%zufrieden_3%' then 3
    when tp.RESPLINK like'%zufrieden_2%' then 2
    when tp.RESPLINK like'%zufrieden_1%' then 1
    when tp.RESPLINK like'%zufrieden_0%' then 0
    else 999 end as RATE_CODE
  , tp.RESP_TIMESTAMP_DAY
  , tp.ACTIVITY_ID||'_'||tp.CUST_ID as VERKN_CLICK
from TOOM_AIC_CAMP.AICV_RESPLINKCATEGORY_ACTIVITY tp
where tp.ACTIVITY_ID IN (
select
distinct sv.ACTIVITY_ID
from TOOM_CM_CORE.D_ACTIVITY sv
where sv.ACTIVITY_NAME = 'E-Mail-Versand_Net_Promoter_Score_Mailing'
) and
 tp.RESPLINK LIKE '%zufrieden%'
 order by tp.ACTIVITY_ID desc, tp.CUST_ID desc, tp.RESP_TIMESTAMP_DAY desc
 ) a
 group by a.DWH_CUSTOMER_ID ;
 
 ---------------------TOTAL JOIN NPS MIN /MAX AND REALOCATED FLAG-------------------------------------------------------------------
DROP TABLE TMP_CUST_NPSSUBTOTAL_TEST PURGE ;
CREATE TABLE TMP_CUST_NPSSUBTOTAL_TEST nologging parallel
as
select
tmp.*
,case when tmp.LAST_RELOCATED_DATE is not null then 1 else 0 end as RELOCATED_FLAG
,nps.MAX_RATE_CODE	
,case when nps.MAX_RATE_CODE > nps.MIN_RATE_CODE	
      then nps.MIN_RATE_CODE
  end                                 as MIN_RATE_CODE
from TMP_CUST_DEADFLAG_RDATE_TEST tmp
left join TMP_CUST_NPS_TEST nps
on tmp.DWH_CUSTOMER_ID = nps.DWH_CUSTOMER_ID;
---1.641.968
select
 *--count (*)
from TMP_CUST_NPSSUBTOTAL_TEST  ;
-------------------------------------------------------------------------------------------------------------------------------
DROP TABLE TMP_CUST_FTOTAL_TEST PURGE ;
CREATE TABLE TMP_CUST_FTOTAL_TEST nologging parallel
as
select
b.*	
, d.MAX_DATE_TIME_START	              as NP_DATE
, d.MIN_DATE_TIME_START	              as FP_DATE	
, d.MAX_DATE_TIME_START_PREV1	        as LP_DATE
, d.MAX_DATE_TIME_START_PREV2	        as LP_DATE1
, d.MAX_DATE_TIME_START_PREV3	        as LP_DATE2
, d.MAX_DATE_TIME_START_PREV4	        as LP_DATE3
, d.TRX_DIFF_DAYS1	                  as NP_DISTANCE
, d.TRX_DIFF_DAYS2	                  as RECENCY
, d.TRX_DIFF_DAYS3	                  as LP_DISTANCE2
, d.TRX_DIFF_DAYS4                    as LP_DISTANCE3

from TMP_CUST_NPSSUBTOTAL_TEST b
inner join TMP_CUST_TRX_LASTDIFF1_DAYS d
on b.DWH_CUSTOMER_ID = d.DWH_CUSTOMER_ID;
-----------------------------------------------------------------------------------------------------------------------1.641.975
select
count (*)
from TMP_CUST_FTOTAL_TEST ;

select
count (*) -- 1.641.968
from TMP_CUST_FTOTAL_TEST ;

--------------------------------------------------------------------------------------------------------------------------------
----------------------------------------Füge alles zusammen als Endergebnis---------------------------------------------------------------------

CREATE TABLE TMP_CUST_TOTAL_TEST NOLOGGING PARALLEL 
as
select
*
from TMP_CUST_FTOTAL_TEST  total1
inner join TMP_CUSTAGG_BOND_RDATE_TEST bline2
on total1.DWH_CUSTOMER_ID = bline2.DWH_CUSTOMER_ID ;












    